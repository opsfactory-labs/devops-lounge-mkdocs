# The DevOps Lounge

This is the website for the The DevOps Lounge Discord community. We're a professional community that
helps professionals and beginners find a safe, well kept community to share ideas, knowledge and
meet other people inside (and outside) the industry.

You can join us [using the following link](https://discord.gg/MTzBvSS). Please review the rules in
the #welcome channel after joining.
