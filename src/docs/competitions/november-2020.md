Welcome to the November 2020 competition!

This month we'll be focusing on automated OS and container patching with Packer and CI/CD. Let's go into some details.

## The Challenge: Automated Patching

The act of patching software means to download and install the latest version of the software you currently have installed on your system. Running the latest version of as piece of software has many benefits which are primarily performance, features and security. When we implement infrastructure we're either running VMs or containers (in most organisations at least) which means there is some layer of operating system and software patching we have to do.

In this month's challenge we're going to build out Packer and Docker workflows that can be used to continuously produce up to date, patched VMs and Docker images. With these images we're going to be deploying a simple WordPress installation: the application server and a database server. We'll do this with "traditional" VMS and again with a more modern containerised solution. Both will have to have a CI solution.

These challenges will involve:

* Designing Packer templates that can build two Ubuntu 20 AWS AMIs for us
* Designing `Dockerfile`s to containerise WordPress and MySQL
* Creating Git repositories for the code and `Dockerfile`s
* Setting up CI pipelines with schedules
* Demonstrating the images being used via something like Terraform
* ... anything else you think you can throw in!

## Objectives

These are the objectives you have to meet before your submission will be considered:

1. Your Packer templates should be portable
1. Your `Dockerfile`s should be portable
1. Your CI pipeline should be reproducable by the judges
1. All code produced has to be idempotent
1. You have to use common tools such as Terraform, Ansible, AWS, GCP, etc
1. Write documentation to explain your work to others
1. Your CI pipelines have to operate on a schedule
1. Your CI pipelines and code have to produce the relevant images
1. You have to demonstrate your final results are working as expected

### Warnings!

1. Be careful not to share credentials in your source code (use environment variables)
1. Remember that creating real resources in public Cloud accounts can result in financial obligations between you and the provider
1. If you're working with someone else be careful how much access you give them to your account(s)

## Rules

These are the rules that you must follow for your submission to be successful:

1. No copy/pasting from Google results such as Stack Overflow, Server Fault, Blogs, etc
1. You CAN work with other people if you wish, but your submission won't be eligible for prizes
1. Your source must be open source via whatever permissive license you wish
1. Your source must be hosted in a Git repository on a public Git hosting site such as GitHub, GitLab, etc
1. You cannot squash your commits - we want to see your thinking and how you worked through problems
1. We cannot pay for any costs you incur directly or indirectly as a result of this competition
1. We don't believe in spoon feeding, so if you assist someone do so in private
