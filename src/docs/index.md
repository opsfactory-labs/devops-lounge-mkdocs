# Welcome to The DevOps Lounge!

This is a Discord based community focused on learning and sharing all things DevOps (and related disciplines, like CloudOps, Agile/Scrum/Kanban, and more).

We welcome anyone and everyone who wants to learn to become a DevOp; who already is a DevOp; who wants to teach others; or just get career advice - we welcome it all.

If you want to join the community then we have an Discord invite code for you: [click here](https://discord.gg/MTzBvSS).

Please ensure you make yourself familiar with [the rules](rules.md).

## Information

For more information about the server and the community check out the following pages:

- [The Rules](rules.md)
- [The Staff](staff.md)
- [Promotion](promotion.md)
- [Competitions](competitions.md)
- [Bots](bots.md)
